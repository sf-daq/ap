#!/bin/bash

cd output
for i in `ls *base.out 2>/dev/null`
do
    if [ -s $i ]
    then
        f=`echo $i | sed 's/\.out//'`
        cp $f a.sh
        chmod +x a.sh
        cd ..
        output/a.sh > output/$i.out 2>&1
        cd output
        rm -rf a.sh
        rm -rf $i
        if [ -s $i.out ]
        then
           mv $i.out $i
        else
           rm -rf $i.out
        fi
    else
        rm -rf $i
    fi
done
cd ..

